terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }
  backend "http" {}
}

variable aws_access_key {}
variable aws_secret_key {}

provider "aws" {
  profile = "default"
  region  = "us-east-1"
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
}

resource "aws_s3_bucket" "slfotg-gitlab-terraform-bucket2" {
  bucket = "slfotg-gitlab-terraform-bucket2"
  acl    = "private"

  tags = {
    Name        = "Gitlab Terraform Test Bucket"
    Environment = "Dev"
  }
}